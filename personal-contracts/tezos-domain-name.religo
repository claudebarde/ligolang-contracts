type storage = {
  addresses: big_map (address,string),
  fee: nat,
  owner: address
};

type return = (list(operation), storage);
type list_of_operation = list(operation);

type action = 
| AddAddress (string)
| UpdateFee (nat)

let addAddress = ((text, storage) : (string, storage)): return => {
  if(Tezos.amount / 1mutez < storage.fee) {
    failwith("Insufficient funds provided!"): return;
  } else {
    ([]: list_of_operation, 
    {...storage, 
      addresses: Big_map.update(Tezos.source, Some (text), storage.addresses)
    });
  }
}

let updateFee = ((newFee, storage) : (nat, storage)): return => {
  if(Tezos.sender != storage.owner) {
    failwith("You are not allowed to change the fee!"): return;
  } else {
    ([]: list_of_operation, {...storage, fee: newFee});
  }
}

let main = ((p,storage): (action, storage)): return => {
  switch (p) {
    | AddAddress (text) => addAddress ((text, storage))
    | UpdateFee (newFee) => updateFee ((newFee, storage))
  }
};
